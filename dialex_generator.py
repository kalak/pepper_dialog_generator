#!/usr/bin/env python
# coding: utf-8

"""
Version 0.0.1
"""
import argparse
import os


AVAILABLE_LANGUAGE = ('frf', 'enu', )
DIALEX = {'dialog_', 'lexicon_', }


def compose_name(args):
    """
    * Parameters:
    args's variables: name and lang
    * Effect:
    Concatenate the name and the lang
    * Return:
    String
    """
    return('{}{}{}'.format(args.name, '_', args.lang))


def lang_in_f_name(include):
    """
    * Parameters:
    include: name of lexicon file
    * Effect:
    Check if the mention of language exists
    * Return:
    Boolean
    """
    checker = False
    for lang in AVAILABLE_LANGUAGE:
        if checker is False:
            checker = lang in include
    return(checker)


def valid_include(all_include):
    """
    * Parameters:
    all_include: included files with the option -i
    * Effect:
    This method checkes if the name of the include files is correct
    * Return:
    Boolean
    """
    checker = True
    try:
        for include in all_include:
            if include[:8] != 'lexicon_':
                print(('Best practise : include only lexicon file not another '
                       'dialog: {}'
                       ).format(include))
                checker = False
            if lang_in_f_name(include) is False:
                print(('Best practise: indicate the current language of '
                       'lexicon in the file name: {}'
                       ).format(include))
                checker = False
            if include[-4:] != '.top':
                print(('You need to include a .top file not just the concept:'
                       ' {}'
                       ).format(include))
                checker = False
    except:
        print('NOTA: during the generation, you can add several include files')
    finally:
        return(checker)


def add_include(f, args):
    """
    * Parameters:
    f: destination file
    args and particular incl, the list of the include files
    * Effect:
    Write in the destination file the different includes
    * Return:
    None
    """
    try:
        for include in args.incl:
            f.write(('include: {}\n').format(include))
    except:
        pass
    else:
        f.write('\n')


def add_header(f, args, type_file):
    """
    * Parameters:
    f: the destination file already open
    args and his variable
    type_file: lexicon_ or dialog_
    * Effect:
    Write in the file the good header
    * Return:
    None
    """
    f.write(('{0}{1}{0}\n'
             '{0} Version 0.0.1\n'
             '{0}{1}{0}\n'
             'topic: ~{2} ()\n'
             'language: {3}\n'
             ).format('#', '=' * 77,
                      ('{}{}').format(type_file, compose_name(args)),
                      args.lang))


def add_concept(f, type_file):
    """
    * Parameters:
    f: the destination file already open
    type_file: lexicon_ or dialog_
    * Effect:
    Write the base of the redaction of concept
    * Return:
    None
    """
    if type_file == 'lexicon_':
        f.write(('concept:() [\n\n]\n'
                 '{0}{1}{0}\n'
                 ).format('#', '-' * 77))
    else:
        f.write(('concept:(output_) [\n\n]\n\n'
                 'u:(~)\n^rand[~output_]\n'
                 '{0}{1}{0}\n'
                 ).format('#', '-' * 77))


def create_file(args, type_file):
    """
    * Paramters:
    args and his different variables
    type_file: lexicon_ or dialog_
    * Effect:
    Create successively the lexicon and dialog files
    * Return:
    None
    """
    f_name = '{}{}{}'.format(type_file, compose_name(args), '.top')
    with open(f_name, 'a') as f:
        if not os.path.getsize(f_name):
            add_header(f, args, type_file)
            add_include(f, args)
        else:
            print(('{} already exists. Just add new concept.'
                   ).format(type_file))
        add_concept(f, type_file)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--name', type=str,
                        default='***',
                        help='Name of project')
    parser.add_argument('-l', '--lang', type=str,
                        default='frf',
                        help='frf or enu')
    parser.add_argument('-i', '--incl', nargs='+',
                        help='Type a serial of include files',
                        required=False)
    args = parser.parse_args()
    if args.lang not in AVAILABLE_LANGUAGE:
        print('Need to choice an available language:\n' + AVAILABLE_LANGUAGE)
        exit(-1)
    if valid_include(args.incl) is False:
        print('Need to concentrate your attention on include files')
        exit(-1)
    try:
        for type_file in DIALEX:
            create_file(args, type_file)
    except:
        print('Check if DIALEX exists')
